package com.justonemoreline.standardaccountactivityapi.dmwatch.controller;

import com.justonemoreline.standardaccountactivityapi.dmwatch.core.databaseDTO.DirectMessageDatabase;
import com.justonemoreline.standardaccountactivityapi.dmwatch.repository.DirectMessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;

@Controller
public class AccActAPIController {
    DirectMessageRepo directMessageRepo;

    @Autowired
    public AccActAPIController(DirectMessageRepo directMessageRepo) {
        this.directMessageRepo = directMessageRepo;
    }

    @RequestMapping(path = "/pingMe")
    public ResponseEntity amIAlive() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/")
    public String home(Model model) {
        List<DirectMessageDatabase> directMessageDatabases = directMessageRepo.findAll();
        model.addAttribute("dms", directMessageDatabases);
        return "home";
    }
}
