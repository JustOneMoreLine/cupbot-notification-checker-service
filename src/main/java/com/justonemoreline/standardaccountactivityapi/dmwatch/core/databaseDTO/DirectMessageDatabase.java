package com.justonemoreline.standardaccountactivityapi.dmwatch.core.databaseDTO;

import lombok.NoArgsConstructor;
import twitter4j.DirectMessage;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@Table(name = "dmHistory")
@Entity
public class DirectMessageDatabase {
    @Id
    private long senderTwitterId;
    @Column
    private DirectMessage directMessage;

    public DirectMessageDatabase(long senderTwitterId,
                                 DirectMessage directMessage) {
        this.senderTwitterId = senderTwitterId;
        this.directMessage = directMessage;
    }

    public void setSenderTwitterId(long senderTwitterId) {
        this.senderTwitterId = senderTwitterId;
    }

    public long getSenderTwitterId() {
        return senderTwitterId;
    }

    public void setDirectMessage(DirectMessage directMessage) {
        this.directMessage = directMessage;
    }

    public DirectMessage getDirectMessage() {
        return directMessage;
    }
}
