package com.justonemoreline.standardaccountactivityapi.dmwatch.core;

import org.springframework.stereotype.Component;
import twitter4j.DirectMessageList;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import java.util.Map;

@Component
public class Twitter4JAdapterImpl implements Twitter4JAdapter {
    Twitter twitter;

    public Twitter4JAdapterImpl() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("hoeuB5Uyes0fdVaZTw87S57wT")
                .setOAuthConsumerSecret("i13Nged9nHOzy4aMO87SQixbRREgYRUlUfnixQuAk0vYgDMxUa")
                .setOAuthAccessToken("1247058788189782016-Jt0eKDNSk9QdKFWfAaRpFa8IsJpcnl")
                .setOAuthAccessTokenSecret("9HfoXYKpf2rACV34FxPNWJdoMJoxvOKNiAlYgkwxeeQdd");
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        this.twitter = twitter;
    }

    @Override
    public DirectMessageList getDirectMessages(int count) {
        try {
            return twitter.getDirectMessages(count);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Map<String, RateLimitStatus> getRateLimitStatus() {
        try {
            return twitter.getRateLimitStatus();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Map<String, RateLimitStatus> getRateLimitStatus(String[] resources) {
        try {
            return twitter.getRateLimitStatus(resources);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Long getMyId() {
        try {
            return twitter.getId();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }
}
