package com.justonemoreline.standardaccountactivityapi.dmwatch.core.jsonTemplates;

public class DirectMessageJson {
    private long dmId;
    private long senderID;
    private long receiverID;
    private String text;

    public void setDmID(long dmID) {
        this.dmId = dmID;
    }

    public Long getDmID() {
        return dmId;
    }

    public void setSenderID(long senderID) {
        this.senderID = senderID;
    }

    public Long getSenderID() {
        return senderID;
    }

    public void setReceiverID(long receiverID) {
        this.receiverID = receiverID;
    }

    public Long getReceiverID() {
        return receiverID;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "DirectMessageJson{" +
                "DmID=" + dmId +
                ", senderID=" + senderID +
                ", receiverID=" + receiverID +
                ", text='" + text + '\'' +
                '}';
    }
}
