package com.justonemoreline.standardaccountactivityapi.dmwatch.core;

import com.justonemoreline.standardaccountactivityapi.dmwatch.core.jsonTemplates.CrcToken;
import org.springframework.web.client.RestTemplate;

/**
 * A class use to handle authentication of this service's HTTP request to other services.
 */
public class ResponseTokenGenerator {

    /**
     * Get's a service's challange token from it hostname, and creates a
     * response token to be use in a HTTP request to that service.
     *
     * @param hostname , a string that contains service's hostname.
     * @return a string of response token.
     */
    public static String generate(String hostname) {
        String challangeUri = "https://" + hostname + "/challange";
        RestTemplate ask = new RestTemplate();
        CrcToken crcToken = ask.getForObject(challangeUri, CrcToken.class);
        String response = Sha256.hash(crcToken.getToken());
        return response;
    }//
}
