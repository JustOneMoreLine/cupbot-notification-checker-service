package com.justonemoreline.standardaccountactivityapi.dmwatch.core;

import twitter4j.DirectMessageList;
import twitter4j.RateLimitStatus;
import java.util.Map;

public interface Twitter4JAdapter {
    public Long getMyId();
    public DirectMessageList getDirectMessages(int count);
    public Map<String, RateLimitStatus> getRateLimitStatus();
    public Map<String, RateLimitStatus> getRateLimitStatus(String[] resources);
}
