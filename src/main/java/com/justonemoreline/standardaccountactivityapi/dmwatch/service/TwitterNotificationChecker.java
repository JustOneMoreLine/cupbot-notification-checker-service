package com.justonemoreline.standardaccountactivityapi.dmwatch.service;

import com.justonemoreline.standardaccountactivityapi.dmwatch.core.ResponseTokenGenerator;
import com.justonemoreline.standardaccountactivityapi.dmwatch.core.Twitter4JAdapter;
import com.justonemoreline.standardaccountactivityapi.dmwatch.core.databaseDTO.DirectMessageDatabase;
import com.justonemoreline.standardaccountactivityapi.dmwatch.core.jsonTemplates.DirectMessageJson;
import com.justonemoreline.standardaccountactivityapi.dmwatch.repository.DirectMessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import twitter4j.DirectMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class TwitterNotificationChecker {
    Twitter4JAdapter twitter;
    String drmsUrl;
    DirectMessageFilter filter;
    DirectMessageRepo directMessageRepo;
    DiscoveryClient discoveryClient;
    private static final String[] asyncKeywords =
            {"!showMyCafe"};

    @Autowired
    public TwitterNotificationChecker(Twitter4JAdapter twitter,
                                      DirectMessageFilter filter,
                                      DirectMessageRepo directMessageRepo,
                                      DiscoveryClient discoveryClient) {
        this.twitter = twitter;
        this.filter = filter;
        this.directMessageRepo = directMessageRepo;
        this.discoveryClient = discoveryClient;
    }
    @Scheduled(fixedDelay = 90000)
    public void checkForNewDms() {
        if (drmsUrl == null) {
            checkDrms();
        } else {
            List<DirectMessage> allDms = twitter.getDirectMessages(50);
            allDms = revertDm(allDms);
            List<DirectMessage> filteredDms = removeDmsByMe(allDms, twitter.getMyId());
            checkNewDms(filteredDms);
        }
    }

    private void checkDrms() {
        List<String> availableService = discoveryClient.getServices();
        if (availableService.contains("directmessageservice")) {
            this.drmsUrl = discoveryClient.getInstances("directmessageservice").get(0).getUri().getHost();
            System.out.println("DIRECTMESSAGESERVICE REACHED AT: " + drmsUrl);
        } else {
            //System.out.println("DIRECTMESSAGESERVICE NOT REACHED");
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
            checkDrms();
        }
    }

    private List<DirectMessage> removeDmsByMe(List<DirectMessage> allDms, long myId) {
        List<DirectMessage> filteredDms = new ArrayList<>();
        for (DirectMessage dm : allDms) {
            if (dm.getSenderId() == myId) {
                continue;
            }
            filteredDms.add(dm);
        }
        return filteredDms;
    }

    private List<DirectMessage> revertDm(List<DirectMessage> listOfDms) {
        List<DirectMessage> result = new ArrayList<>();
        int listOfDmIndex = listOfDms.size() - 1;
        for (int i = 0; i <= listOfDmIndex; i++) {
            result.add(listOfDms.get(listOfDmIndex - i));
        }
        return result;
    }

    private void checkNewDms(List<DirectMessage> dms) {
        List<DirectMessage> asyncDms = new ArrayList<>();
        List<DirectMessage> syncDms = new ArrayList<>();
        for (DirectMessage dm : dms) {
            long senderId = dm.getSenderId();
            List<DirectMessageDatabase> listOfDmsByThisId =
                    directMessageRepo.findDirectMessageDatabaseBySenderTwitterId(senderId);
            if (listOfDmsByThisId.isEmpty()) {
                saveNewDmToDatabase(dm, senderId);
                if (dmIsAsync(dm)) {
                    asyncDms.add(dm);
                } else {
                    syncDms.add(dm);
                }
            } else if (listOfDmsByThisId.size() == 1) {
                if (checkIfItsANewDm(dm, senderId)) {
                    if (dmIsAsync(dm)) {
                        asyncDms.add(dm);
                    } else {
                        syncDms.add(dm);
                    }
                }
            }
        }
        /*System.out.println("===========ASYNC==============");
        for (DirectMessage dm : asyncDms) {
            System.out.println(dm.getText());
        }
        System.out.println("===========SYNC==============");
        for (DirectMessage dm : syncDms) {
            System.out.println(dm.getText());
        }*/
        respondAsyncDms(asyncDms);
        respondSyncDms(syncDms);
    }

    private boolean dmIsAsync(DirectMessage dm) {
        String text = dm.getText();
        boolean dmIsAsync = false;
        for (String keyword : asyncKeywords) {
            if (text.contains(keyword)) {
                dmIsAsync = true;
                break;
            }
        }
        return dmIsAsync;
    }

    private void saveNewDmToDatabase(DirectMessage dm, long senderId) {
        DirectMessageDatabase newDm = new DirectMessageDatabase(senderId, dm);
        directMessageRepo.saveAndFlush(newDm);
    }

    private boolean checkIfItsANewDm(DirectMessage dm, long senderId) {
        DirectMessageDatabase dmFromDatabaseEntity =
                directMessageRepo.findDirectMessageDatabaseBySenderTwitterId(senderId).get(0);
        DirectMessage dmInDatabase = dmFromDatabaseEntity.getDirectMessage();
        if (dmInDatabase.getCreatedAt().before(dm.getCreatedAt())) {
            directMessageRepo.delete(dmFromDatabaseEntity);
            saveNewDmToDatabase(dm, senderId);
            return true;
        } else {
            return false;
        }
    }

    @Async
    public void respond(DirectMessage dm) {
        String text = dm.getText();
        boolean requestIsSync = true;
        for (String keyword : asyncKeywords) {
            if (text.contains(keyword)) {
                requestIsSync = false;
                respondAsync(dm);
                break;
            }
        }

        if (requestIsSync) {
            respondSync(dm);
        }
    }

    @Async
    public void respondAsyncDms(List<DirectMessage> dms) {
        for (DirectMessage dm : dms) {
            respondAsync(dm);
        }
    }

    public void respondAsync(DirectMessage dm) {
        System.out.println("ASYNC: " + dm.getText());
        DirectMessageJson packageToSent = setUpPackage(dm);
        String response = ResponseTokenGenerator.generate(drmsUrl);
        WebClient client = WebClient.create("https://" + drmsUrl);
        Mono<Void> result = client
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path("/asyncWebhook")
                        .queryParam("response", response)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(packageToSent)
                .retrieve()
                .bodyToMono(Void.class);
        result.subscribe();
    }

    @Async
    public void respondSyncDms(List<DirectMessage> dms) {
        for (DirectMessage dm : dms) {
            CompletableFuture<Void> requestStatus = respondSync(dm);
            while (!requestStatus.isDone()) {
                System.out.println("ONGOING: " + dm.getText());
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            System.out.println("DONE: " + dm.getText());
        }
    }

    private CompletableFuture<Void> respondSync(DirectMessage dm) {
        System.out.println("SYNC: " + dm.getText());
        DirectMessageJson packageToSent = setUpPackage(dm);
        String response = ResponseTokenGenerator.generate(drmsUrl);
        WebClient client = WebClient.create("https://" + drmsUrl);
        CompletableFuture<Void> result = client
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path("/webhook")
                        .queryParam("response", response)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(packageToSent)
                .retrieve()
                .bodyToMono(Void.class)
                .toFuture();
        return result;
    }

    private DirectMessageJson setUpPackage(DirectMessage dm) {
        DirectMessageJson packageToSent = new DirectMessageJson();
        packageToSent.setDmID(dm.getId());
        packageToSent.setSenderID(dm.getSenderId());
        packageToSent.setReceiverID(dm.getRecipientId());
        packageToSent.setText(dm.getText());
        return packageToSent;
    }
}
