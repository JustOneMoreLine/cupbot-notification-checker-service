package com.justonemoreline.standardaccountactivityapi.dmwatch.service;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import java.util.List;

@Component
@EnableAsync
public class DirectMessageFilter {

    public List<DirectMessage> filterFromSender(DirectMessageList list, long senderId) {
        for (DirectMessage x : list) {
            if (senderId == x.getSenderId()) {
                list.remove(x);
            }
        }
        return list;
    }

    public List<DirectMessage> filterFromSender(List<DirectMessage> list, long senderId) {
        for (int i = 0; i < list.size(); i++) {
            if (senderId == list.get(i).getSenderId()) {
                list.remove(list.get(i));
            }
        }
        return list;
    }

    public List<DirectMessage> filterFromRecipient(List<DirectMessage> list, long receipId) {
        for (DirectMessage x : list) {
            if (receipId == x.getId()) {
                list.remove(x);
            }
        }
        return list;
    }

    public List<DirectMessage> filterFromRecipient(DirectMessageList list, long receipId) {
        for (DirectMessage x : list) {
            if (receipId == x.getId()) {
                list.remove(x);
            }
        }
        return list;
    }
}
