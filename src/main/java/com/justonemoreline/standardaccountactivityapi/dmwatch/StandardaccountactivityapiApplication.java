package com.justonemoreline.standardaccountactivityapi.dmwatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
@EnableAsync
@EnableEurekaClient
public class StandardaccountactivityapiApplication {
    public static void main(String[] args) {
        SpringApplication.run(StandardaccountactivityapiApplication.class, args);
    }
}
