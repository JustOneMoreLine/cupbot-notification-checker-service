package com.justonemoreline.standardaccountactivityapi.dmwatch.repository;

import com.justonemoreline.standardaccountactivityapi.dmwatch.core.databaseDTO.DirectMessageDatabase;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface DirectMessageRepo extends JpaRepository<DirectMessageDatabase, Long> {
    List<DirectMessageDatabase> findDirectMessageDatabaseBySenderTwitterId(long senderTwitterId);
}
